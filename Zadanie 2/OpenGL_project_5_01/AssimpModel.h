#pragma once

#include <vector>
#include "gl3w.h"
#include "glm\glm.hpp"
#include <GLFW\glfw3.h>
#include <assimp\scene.h>
#include <assimp\mesh.h>

class AssimpModel
{
public:
	AssimpModel(const char* filename, const char* texturePath);
	~AssimpModel();
	void Render();

	struct MeshData
	{
		static enum BUFFERS{
			BUFFER_VERTEX, BUFFER_TEXCOORD, BUFFER_NORMALS, BUFFER_INDEX,
			NUM_BUFFERS
		};
		GLuint VAO;
		GLuint VBO[4];

		unsigned int elementCount;
		MeshData(aiMesh * mesh);
		~MeshData();

		void Load(aiMesh * mesh);
		void Render();
	};

	GLuint texture;
	glm::mat4 modelMatrix;

	std::vector<MeshData*> meshData;
};

