#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>
#include <SOIL\SOIL.h>
#define LoadTextureFromFile(X) SOIL_load_OGL_texture(X, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);


/*Classes*/
#include "Mesh.h"
#include "system\Application.h"
#include "system\Camera.h"
#include "AssimpModel.h"

//---------------------------------------------------------
void StartGL();
void QuitGL();

class VaryingShader : public EventListener
{
public:
	VaryingShader()
	{
		addEventListener(EventType::E_KEYBOARD_EVENT);
	}
	~VaryingShader()
	{
		EventListener::~EventListener();
		delete planar;
		delete spherical;
	}

	virtual void onEvent(const Event* e)
	{
		const KeyboardEvent* keyEvent = (const KeyboardEvent*)e;
		if (keyEvent->key == GLFW_KEY_SPACE && keyEvent->action == GLFW_RELEASE)
		{
			bPlanar = !bPlanar;
		}
	}
	Shader* getShader()
	{
		return bPlanar ? planar : spherical;
	}
	void ParseDataToShader(Light light, glm::mat4 camera, glm::mat4 model, GLuint texture)
	{
		GLuint shader = getShader()->GetProgramID();

		glUniform3fv(glGetUniformLocation(shader, "light.lightPosition"), 1, &light.lightPosition[0]);
		glUniform3fv(glGetUniformLocation(shader, "light.lightColor"), 1, &light.lightColor[0]);
		glUniformMatrix4fv(glGetUniformLocation(shader, "camera"), 1, GL_FALSE, &camera[0][0]);
		glUniformMatrix4fv(glGetUniformLocation(shader, "transform"), 1, GL_FALSE, &model[0][0]);
		glUniform1i(glGetUniformLocation(shader, "tex"), texture);
	}
	void Bind()
	{
		getShader()->Bind();
	}
private:
	bool bPlanar = true;
	Shader * planar = new Shader("shaders/planarProjection");
	Shader * spherical = new Shader("shaders/sphericalProjection");
};

class TwoTransforms : public EventListener
{
public:
	TwoTransforms()
	{
		addEventListener(EventType::E_KEYBOARD_EVENT);
	}
	~TwoTransforms()
	{
		EventListener::~EventListener();
	}
	
	virtual void onEvent(const Event* e)
	{
		const KeyboardEvent* keyEvent = (const KeyboardEvent*)e;
		if (keyEvent->action == GLFW_RELEASE)
		{
			switch (keyEvent->key)
			{
			case GLFW_KEY_ENTER:
				bModyfingFirst = !bModyfingFirst;
				break;
			case GLFW_KEY_EQUAL:
				(bModyfingFirst ? scale1 : scale2) += 0.1f;
				break;
			case GLFW_KEY_MINUS:
				(bModyfingFirst ? scale1 : scale2) -= 0.1f;
				break;
			case GLFW_KEY_Z:
				(bModyfingFirst ? rotation1.z : rotation2.z) += 0.1f;
				break;
			case GLFW_KEY_Y:
				(bModyfingFirst ? rotation1.y : rotation2.y) += 0.1f;
				break;
			case GLFW_KEY_X:
				(bModyfingFirst ? rotation1.x : rotation2.x) += 0.1f;
				break;
			}
		}
	}
	glm::mat4 getMatrix1()
	{
		return glm::scale(glm::vec3(scale1))*CreateRotationMatrix(true);
	}
	glm::mat4 getMatrix2()
	{
		return glm::scale(glm::vec3(scale2))*CreateRotationMatrix(false);
	}
private:
	glm::vec3 rotation1;
	float scale1 = 1.0f;
	glm::vec3 rotation2;
	float scale2 = 1.0;;
	bool bModyfingFirst = true;
	glm::mat4 CreateRotationMatrix(bool first)
	{
		glm::vec3 angle = first ? rotation1 : rotation2;
		glm::mat4 rot[3];
		rot[0] = glm::mat4(1, 0, 0, 0,
			0, cos(angle.x), -sin(angle.x), 0,
			0, sin(angle.x), cos(angle.x), 0,
			0, 0, 0, 1);
		rot[1] = glm::mat4(cos(angle.y), 0.0f, sin(angle.y), 0.0f,
			0, 1, 0, 0,
			-sin(angle.y), 0, cos(angle.y), 0,
			0, 0, 0, 1);
		rot[2] = glm::mat4(cos(angle.z), -sin(angle.z), 0, 0,
			sin(angle.z), cos(angle.z), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
		return rot[0] * rot[1] * rot[2];
	}
};


int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	VaryingShader shaders;
	TwoTransforms transforms;
	printf("LOADING MODELS...\n");
	AssimpModel model1("files/box.obj", "files/tiger2.jpg");
	AssimpModel model2("files/pawn.obj", "files/texture.jpg");
	printf("FINISHED LOADING MODELS.");
	FPSCamera * camera = new FPSCamera();
	camera->front = -camera->front;
	camera->yaw = -270.0f;


	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);
		camera->onEveryFrame();

		////////////////////////////////
		glm::mat4 cameraMatrix = application.getPerspectiveMatrix()*camera->getCameraMatrix();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, model1.texture);
		model1.modelMatrix = glm::translate(glm::vec3(0,0,10))*transforms.getMatrix1();
		model2.modelMatrix = glm::translate(glm::vec3(-10, 0, 10))*transforms.getMatrix2();
		shaders.ParseDataToShader(application.light, cameraMatrix, model1.modelMatrix, 0);
		shaders.Bind();
		model1.Render();
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, model2.texture);
		shaders.ParseDataToShader(application.light, cameraMatrix, model2.modelMatrix, 1);
		shaders.Bind();
		model2.Render();

		

		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSetCursorPosCallback(application.window, mouse_callback);
	glfwSetMouseButtonCallback(application.window, mouse_button_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(.3, .3, .3, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}