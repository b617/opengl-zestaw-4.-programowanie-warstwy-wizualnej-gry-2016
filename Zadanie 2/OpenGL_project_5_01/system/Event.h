#pragma once
#include <list>
#include <GLFW\glfw3.h>

enum EventType{
	E_ERROR = 0,
	E_MOUSE_EVENT = 1,
	E_KEYBOARD_EVENT = 2,
	E_ENTER_FRAME = 4
};

class EventListener;

class Event
{
private:
protected:
	~Event();
	virtual void dispatchEvent();
public:
	Event();
	virtual EventType getEventType()const{ return E_ERROR; }
};

class KeyboardEvent : public Event
{
public:
	KeyboardEvent(GLFWwindow* window, int key, int scancode, int action, int mods);
	~KeyboardEvent();

	static std::list<EventListener*> listeners;
	GLFWwindow* window;
	int key;
	int scancode;
	int action;
	int mods;

	virtual EventType getEventType()const{ return E_KEYBOARD_EVENT; }
protected:
	virtual void dispatchEvent();
};

class MouseEvent : public Event
{
public:
	MouseEvent(GLFWwindow * window, int button, int action, int mods);
	MouseEvent(GLFWwindow* window, double xPos, double yPos);
	~MouseEvent();

	static std::list<EventListener*> listeners;

	enum type{
		E_MOUSE_MOVED,
		E_MOUSE_CLICKED
	};

	GLFWwindow* window;
	double xPos=0.0;
	double yPos=0.0;

	int button=0;
	int action=0;
	int mods=0;

	type type;
	virtual EventType getEventType()const{ return E_MOUSE_EVENT; }
protected:
	virtual void dispatchEvent();
};

class EnterFrameEvent : public Event
{
public:
	EnterFrameEvent(GLFWwindow*);
	~EnterFrameEvent();

	static std::list<EventListener*> listeners;
	GLFWwindow* window;
	virtual EventType getEventType()const{ return E_ENTER_FRAME; }
protected:
	virtual void dispatchEvent();
};

class EventListener
{
public:
	EventListener();
	virtual ~EventListener();

	virtual void onEvent(const Event*);
	void addEventListener(EventType);
private:
	unsigned int listensTo = 0;
};