#pragma once
#include "..\glm\glm.hpp"
struct Light{
	glm::vec3 lightPosition;
	glm::vec3 lightColor;
};