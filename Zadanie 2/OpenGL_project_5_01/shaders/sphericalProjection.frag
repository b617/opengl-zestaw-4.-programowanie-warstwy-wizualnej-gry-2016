#version 140

uniform mat4 transform;
uniform sampler2D tex;

uniform struct Light
{
	vec3 lightPosition;
	vec3 lightColor;
} light;

in vec2 fragTexCoord;
in vec3 fragNormal;
in vec3 fragPosition;

void main()
{
	vec3 ambientColor=0.3f*light.lightColor;
	
	vec3 normal=normalize(fragNormal);
	vec3 lightDirection=normalize(light.lightPosition-fragPosition);
	
	float brightness=max(dot(normal,lightDirection),0.0);
	vec3 diffuse = brightness*light.lightColor;
	
	vec3 outputColor=(ambientColor+diffuse)*vec3(1.0);
	
	vec4 texPixel=texture(tex,fragTexCoord);
	gl_FragColor=vec4(outputColor*texPixel.rgb,texPixel.a);
}