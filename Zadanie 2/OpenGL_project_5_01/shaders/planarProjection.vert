#version 140
 
uniform mat4 camera;
uniform mat4 transform;
attribute vec3 position;

in vec3 normal;

out vec3 fragPosition;
out vec2 fragTexCoord;
out vec3 fragNormal;

void main()
{
	
	fragPosition=position;
	fragNormal=normal;
	fragTexCoord=vec2(position.x,position.y);


    gl_Position = camera * transform * vec4(position,1.0);
}