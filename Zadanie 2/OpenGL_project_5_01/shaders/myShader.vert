#version 140
 
uniform vec3 inColor;
uniform mat4 transform;
attribute vec3 position;

void main()
{
    gl_Position = transform * vec4(position,1.0);
}