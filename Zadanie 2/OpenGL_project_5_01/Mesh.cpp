#include "stdafx.h"
#include "Mesh.h"
#include <SOIL\SOIL.h>


Mesh::Mesh(Vertex* vertices, unsigned int numVertices, GLuint drawingMode) : mDrawingMode(drawingMode), mDrawCount(numVertices)
{
	glGenVertexArrays(1, &mVertexArrayObject);
	glBindVertexArray(mVertexArrayObject);

		glGenBuffers(NUM_BUFFERS, mVertexArrayBuffers);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(vertices[0]),vertices,GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}
Mesh::Mesh(std::vector<Vertex> & vertices, GLuint drawingMode) : mDrawingMode(drawingMode)
{
	glGenVertexArrays(1, &mVertexArrayObject);
	glBindVertexArray(mVertexArrayObject);

		glGenBuffers(NUM_BUFFERS, mVertexArrayBuffers);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}

Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &mVertexArrayObject);
}

void Mesh::Draw() {
	glBindVertexArray(mVertexArrayObject);
		
		glDrawArrays(mDrawingMode, 0, mDrawCount);
		
	glBindVertexArray(0);
}

Mesh & Mesh::setDrawingMode(GLuint value) {
	mDrawingMode = value;
	return *this;
}


HeightMap::HeightMap(const char* filename){
	int width, height, channels;
	unsigned char* ht_map = SOIL_load_image(filename, &width, &height, &channels, SOIL_LOAD_L);
	numVertices = height*width;
	texture = SOIL_load_OGL_texture("files/terrain/colormap.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, NULL);
	glm::vec3* vertices = new glm::vec3[numVertices]();

	//wierzcholki
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			vertices[j*width + i] = glm::vec3(i, j, ht_map[j*width + i]);
		}
	}

	//normale
	glm::vec3* normals = new glm::vec3[numVertices];


	for (int i = 0; i < numVertices; i++)
	{
		normals[i] = glm::vec3(0, 1, 0);
	}

	//ponizszy algorytm opisany na zalaczonym rysunku "normale_gora.png"
	for (int i = 1; i < width - 1; i++)
	{
		for (int j = 1; j < height - 1; j++)
		{
			glm::vec3 najjasniejszy = glm::vec3(i, j, ht_map[j*width + i]), najciemniejszy = glm::vec3(i, j, ht_map[j*width + i]);
			//@up: x,y - koordynaty najjasniejszego/najciemniejszego piksela w okolicy, z - wysokosc
			for (int x = -1; x <= 1; x++)
			{
				for (int y = -1; y <= 1; y++)
				{
					if (ht_map[(j + y)*width + (i + x)]>najjasniejszy.z) najjasniejszy = glm::vec3(i + x, j + y, ht_map[(j + y)*width + (i + x)]);
					if (ht_map[(j + y)*width + (i + x)]<najciemniejszy.z) najciemniejszy = glm::vec3(i + x, j + y, ht_map[(j + y)*width + (i + x)]);
				}
			}
			glm::vec3 normXY = glm::normalize(glm::vec3(najjasniejszy.x, najjasniejszy.y, 0) - glm::vec3(najciemniejszy.x, najciemniejszy.y, 0));
			float diffn = (najjasniejszy.z - najciemniejszy.z) / 255;
			normals[j*width + i] = glm::normalize(glm::vec3(normXY.x*glm::cos(diffn*M_PI), normXY.y*glm::cos(diffn*M_PI), glm::sin(diffn*M_PI)));
		}
	}

	//scianki
	numIndices = numVertices + height - 1;
	unsigned int* faces = new unsigned int[numIndices];
	for (unsigned int i = 0; i < width; i++)
	{
		for (unsigned int j = 0; j < height; j++)
		{
			faces[(width + 1)*j + i] = width*j + i;
		}
	}
	for (int i = 0; i < height - 1; i++)
	{
		faces[(width + 1)*i + width] = GL_PRIMITIVE_RESTART;
	}

	//Create VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	//Create VBO
	glGenBuffers(NUM_BUFFERS, &VBO[POSITION]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[POSITION]);
	glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(vertices[0]), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[NORMALS]);
	glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(normals[0]), normals, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[FACES]);
	glBufferData(GL_ARRAY_BUFFER, (numVertices + height - 1)*sizeof(faces[0]), faces, GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 1, GL_UNSIGNED_INT, GL_FALSE, 0, 0);

	glBindVertexArray(0);

	//Clean temporary data
	delete[] normals;
	delete[] vertices;
	delete[] faces;
	SOIL_free_image_data(ht_map);

	shader = new Shader("shaders/ht_mapShader");
}
HeightMap::~HeightMap()
{
	glDeleteVertexArrays(1, &VAO);
}
void HeightMap::Draw()
{
	glBindVertexArray(VAO);
	glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(GL_PRIMITIVE_RESTART);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, numVertices);
	//glDrawElements(GL_TRIANGLE_STRIP, numIndices, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
void HeightMap::SendDataToShaderAndBind(Light light, glm::mat4 camera)
{
	GLuint shaderProgram = shader->GetProgramID();
	glUniform3fv(glGetUniformLocation(shaderProgram, "light.lightColor"), 1, &light.lightColor[0]);
	glUniform3fv(glGetUniformLocation(shaderProgram, "light.lightPosition"), 1, &light.lightPosition[0]);
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "camera"), 1, GL_FALSE, &camera[0][0]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(texture, 0);
	glUniform1i(glGetUniformLocation(shaderProgram, "tex"), 0);
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_FALSE, &modelMatrix[0][0]);
	glUniform1f(glGetUniformLocation(shaderProgram, "maxHeight"), 255.0f);
	glUniform1f(glGetUniformLocation(shaderProgram, "minHeight"), 0.0f);
	shader->Bind();
}

//////////////////////////////////////////////////

Mesh * BASIC_SHAPES::shapes2D::RegularPolygon(float radius, unsigned int verticesCount){
	return new Mesh(RegularPolygonVertices(radius,verticesCount), verticesCount, GL_TRIANGLE_FAN);
}
Vertex * BASIC_SHAPES::shapes2D::RegularPolygonVertices(float radius, unsigned int verticesCount){
	float angle = M_PI * 2 / verticesCount, currentAngle = 0.0f;
	Vertex * vertices = (Vertex*)malloc(sizeof(Vertex)*verticesCount);

	for (unsigned int i = 0; i < verticesCount; i++, currentAngle += angle) {
		vertices[i] = Vertex(glm::vec3(radius*cos(currentAngle), radius*sin(currentAngle), 0.0f));
	}
	return vertices;
}
Mesh * BASIC_SHAPES::shapes2D::Square(float radius){
	return RegularPolygon(radius, 4);
}
Mesh * BASIC_SHAPES::shapes2D::Triangle(float radius){
	return RegularPolygon(radius, 3);
}

MeshStructure * BASIC_SHAPES::shapes3D::Cone(float radius, float height, unsigned int numVertices){
	MeshStructure * result = new MeshStructure();
	Mesh * side, *base;
	float phi = 2 * M_PI / numVertices;

	//for (float u = 0.0f; u <= 1.0f; u += precision){
	//	for (float v = 0.0f; v <= 1.0f; v += precision){
	//		phi = u*angle;
	//		vertices.push_back(Vertex(glm::vec3(
	//			radius * (1 - v)*cos(phi),
	//			radius * (1 - v)*sin(phi),
	//			v*height)));
	//	}
	//}

	Vertex* vert = (Vertex*)malloc(sizeof(Vertex)*(numVertices + 2));
	vert[0]=(Vertex(glm::vec3(0.0f, 0.0f, height)));
	for (unsigned int i = 0; i <= numVertices; i++){
		vert[i+1]=(Vertex(glm::vec3(cos(phi*i)*radius, sin(phi*i)*radius,0.0f)));
		//fputs((vert[i + 1].toString()+'\n').c_str(), stdout);
	}


	base = BASIC_SHAPES::shapes2D::RegularPolygon(radius,numVertices);
	side = new Mesh(vert, numVertices + 2, GL_TRIANGLE_FAN);

	result->objects.push_back(MeshWithShader(base, NULL));
	result->objects.push_back(MeshWithShader(side, NULL));
	return result;
}
MeshStructure * BASIC_SHAPES::shapes3D::Cylinder(float radius, float height, unsigned int numVertices){
	MeshStructure * result = new MeshStructure();
	Mesh * side, *base1, *base2;
	Vertex * base1Vert, *base2Vert, *sideVert;
	base1Vert = BASIC_SHAPES::shapes2D::RegularPolygonVertices(radius, numVertices);
	base2Vert = BASIC_SHAPES::shapes2D::RegularPolygonVertices(radius, numVertices);
	for (int i = 0; i < numVertices; i++) base2Vert[i].pos.z = height;
	sideVert = (Vertex*)malloc(sizeof(Vertex)* 2 * numVertices +2);

	for (int i = 0; i < numVertices * 2; i += 2) {
		sideVert[i] = base1Vert[i / 2];
		sideVert[i + 1] = base2Vert[i / 2];
	}
	sideVert[numVertices * 2] = base1Vert[0];
	sideVert[numVertices * 2 + 1] = base2Vert[0];

	base1 = new Mesh(base1Vert, numVertices, GL_TRIANGLE_FAN);
	base2 = new Mesh(base2Vert, numVertices, GL_TRIANGLE_FAN);
	side = new Mesh(sideVert, numVertices * 2 +2, GL_TRIANGLE_STRIP);

	result->objects.push_back(MeshWithShader(base1, NULL));
	result->objects.push_back(MeshWithShader(base2, NULL));
	result->objects.push_back(MeshWithShader(side, NULL));
	return result;
}
MeshStructure * BASIC_SHAPES::shapes3D::Sphere(float radius, unsigned int numVertices){
	MeshStructure * result = new MeshStructure();
	Mesh ** sphere = new Mesh*[numVertices - 1];

	float step = 2 * radius / numVertices;
	for (unsigned int i = 0; i < numVertices -1; i++){
		float y = step*i - radius;
		Vertex * bottom = BASIC_SHAPES::shapes2D::RegularPolygonVertices(sqrt(radius*radius-y*y), numVertices);
		for (unsigned int j = 0; j < numVertices; j++) bottom[j].pos.z = y;
		y += step;
		Vertex * top = BASIC_SHAPES::shapes2D::RegularPolygonVertices(sqrt(radius*radius - y*y), numVertices);
		for (unsigned int j = 0; j < numVertices; j++) top[j].pos.z = y;

		Vertex * sideVert = (Vertex*)malloc(sizeof(Vertex)* 2 * numVertices + 2);
		for (unsigned int j = 0; j < numVertices * 2; j += 2) {
			sideVert[j] = bottom[j / 2];
			sideVert[j + 1] = top[j / 2];
		}
		sideVert[numVertices * 2] = bottom[0];
		sideVert[numVertices * 2 + 1] = top[0];
		delete[] bottom; delete[] top;

		sphere[i] = new Mesh(sideVert, numVertices * 2 + 2, GL_TRIANGLE_STRIP);
		result->objects.push_back(MeshWithShader(sphere[i],NULL));
	}

	return result;
}