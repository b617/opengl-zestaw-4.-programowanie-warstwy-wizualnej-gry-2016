#include "stdafx.h"
#include "Event.h"


Event::Event()
{
	dispatchEvent();
}
Event::~Event()
{
}
void Event::dispatchEvent()
{
}

std::list<EventListener*> KeyboardEvent::listeners;
KeyboardEvent::KeyboardEvent(GLFWwindow* window, int key, int scancode, int action, int mods) 
:window(window), key(key), scancode(scancode), action(action), mods(mods), Event()
{
	dispatchEvent();
}
KeyboardEvent::~KeyboardEvent()
{
}
void KeyboardEvent::dispatchEvent()
{
	for each (EventListener* var in listeners)	var->onEvent(this);
}

std::list<EventListener*> MouseEvent::listeners;
MouseEvent::MouseEvent(GLFWwindow * window, int button, int action, int mods)
:window(window), button(button), action(action), mods(mods), type(type::E_MOUSE_CLICKED), Event()
{
	dispatchEvent();
}
MouseEvent::MouseEvent(GLFWwindow* window, double xPos, double yPos)
: window(window), button(button), xPos(xPos),yPos(yPos), type(type::E_MOUSE_MOVED), Event()
{
	dispatchEvent();
}
MouseEvent::~MouseEvent()
{
}
void MouseEvent::dispatchEvent()
{
	for each (EventListener* var in listeners)	var->onEvent(this);
}

std::list<EventListener*> EnterFrameEvent::listeners;
EnterFrameEvent::EnterFrameEvent(GLFWwindow* window) :window(window)
{
	dispatchEvent();
}
EnterFrameEvent::~EnterFrameEvent()
{

}
void EnterFrameEvent::dispatchEvent()
{
	for each (EventListener* var in listeners) var->onEvent(this);
}

EventListener::EventListener()
{
}
EventListener::~EventListener()
{
	if (listensTo & E_MOUSE_EVENT) MouseEvent::listeners.remove(this);
	if (listensTo & E_KEYBOARD_EVENT) KeyboardEvent::listeners.remove(this);
	if (listensTo&E_ENTER_FRAME) EnterFrameEvent::listeners.remove(this);
}
void EventListener::addEventListener(EventType type)
{
	listensTo |= type;
	switch (type)
	{
	case E_MOUSE_EVENT:
		MouseEvent::listeners.push_back(this);
		break;
	case E_KEYBOARD_EVENT:
		KeyboardEvent::listeners.push_back(this);
		break;
	}
}
void EventListener::onEvent(const Event*)
{
}