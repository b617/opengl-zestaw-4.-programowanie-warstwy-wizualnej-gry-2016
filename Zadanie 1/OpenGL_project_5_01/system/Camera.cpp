#include "stdafx.h"
#include "Camera.h"
#include "Application.h"
#include "..\glm\gtc\matrix_transform.hpp"


Camera::Camera()
{
}
Camera::~Camera()
{
}
glm::mat4 Camera::getCameraMatrix()
{
	return glm::lookAt(position, position + front, up);
}

FPSCamera::FPSCamera() :Camera(), EventListener()
{
	for (int i = 0; i < FPSCameraKey_NUM_KEYS; i++) keyIsPressed[i] = false;
	glfwGetCursorPos(Application::GetInstance().window, &mouseX, &mouseY);

	addEventListener(EventType::E_KEYBOARD_EVENT);
	addEventListener(EventType::E_MOUSE_EVENT);
	addEventListener(EventType::E_ENTER_FRAME);

	glfwSetInputMode(Application::GetInstance().window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}
FPSCamera::~FPSCamera()
{
	EventListener::~EventListener();
	Camera::~Camera();
}
void FPSCamera::onEvent(const Event* event)
{
	switch (event->getEventType())
	{
	case E_KEYBOARD_EVENT:
		onKeyEvent((const KeyboardEvent*)event);
		return;
	case E_MOUSE_EVENT:
		onMouseMoved((const MouseEvent*)event);
		return;
	case E_ENTER_FRAME:
		onEveryFrame();
		return;
	}
}
void FPSCamera::onKeyEvent(const KeyboardEvent* event)
{
	if (event->action == GLFW_PRESS)
	{
		switch (event->key)
		{
		case GLFW_KEY_W:
			keyIsPressed[FPSCameraKey_W] = true;
			break;
		case GLFW_KEY_S:
			keyIsPressed[FPSCameraKey_S] = true;
			break;
		case GLFW_KEY_A:
			keyIsPressed[FPSCameraKey_A] = true;
			break;
		case GLFW_KEY_D:
			keyIsPressed[FPSCameraKey_D] = true;
			break;
		}
	}
	else{
		switch (event->key)
		{
		case GLFW_KEY_W:
			keyIsPressed[FPSCameraKey_W] = false;
			break;
		case GLFW_KEY_S:
			keyIsPressed[FPSCameraKey_S] = false;
			break;
		case GLFW_KEY_A:
			keyIsPressed[FPSCameraKey_A] = false;
			break;
		case GLFW_KEY_D:
			keyIsPressed[FPSCameraKey_D] = false;
			break;
		}
	}
}
void FPSCamera::onEveryFrame()
{
	if (keyIsPressed[FPSCameraKey_W])
		position += cameraSpeed*front;
	if (keyIsPressed[FPSCameraKey_S])
		position -= cameraSpeed*front;
	if (keyIsPressed[FPSCameraKey_A])
		position -= glm::normalize(glm::cross(front, up))*cameraSpeed;
	if (keyIsPressed[FPSCameraKey_D])
		position += glm::normalize(glm::cross(front, up))*cameraSpeed;
	if (glfwGetKey(Application::GetInstance().window, GLFW_KEY_SPACE)) system("pause");

	/*LOG*/
	//printf("position: %f,%f,%f\n", position.x, position.y, position.z);
	//printf("front: %f,%f,%f\n\n", front.x, front.x, front.z);
}
void FPSCamera::onMouseMoved(const MouseEvent* event)
{
	GLfloat offsetX = event->xPos - mouseX;
	GLfloat offsetY = mouseY - event->yPos;
	mouseX = event->xPos;
	mouseY = event->yPos;
	offsetX *= rotationSpeed;
	offsetY *= rotationSpeed;
	yaw += offsetX;
	pitch += offsetY;
	if (pitch > 89.0f) pitch = 89.0f;
	if (pitch < -89.0f)pitch = -89.0f;

	glm::vec3 newFront;
	newFront.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	newFront.y = sin(glm::radians(pitch));
	newFront.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(newFront);
}
glm::mat4 FPSCamera::getCameraMatrix()
{
	return glm::lookAt(position, position + front, up);
}

RotatingCamera::RotatingCamera() :Camera()
{
}
RotatingCamera::~RotatingCamera()
{
	Camera::~Camera();
}
glm::mat4 RotatingCamera::getCameraMatrix()
{
	GLfloat camX = sin(glfwGetTime() * speed) * radius;
	GLfloat camZ = cos(glfwGetTime() * speed) * radius;
	return glm::lookAt(glm::vec3(camX, position.y, camZ), position, up);
}
