#pragma once
#include "..\glm\glm.hpp"
#include "Event.h"
class Camera
{
public:
	Camera();
	~Camera();
	virtual glm::mat4 getCameraMatrix();

	glm::vec3 position = glm::vec3(0.0f, 0.0f, 3.0f);
	glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
};

class FPSCamera : public Camera, public EventListener
{
private:
	enum FPSCameraKey{
		FPSCameraKey_W, FPSCameraKey_A, FPSCameraKey_S, FPSCameraKey_D, 
		FPSCameraKey_NUM_KEYS
	};
	bool keyIsPressed[FPSCameraKey_NUM_KEYS];

	void onMouseMoved(const MouseEvent*);
	void onKeyEvent(const KeyboardEvent*);

	double mouseX, mouseY;
public:
	FPSCamera();
	~FPSCamera();

	virtual void onEvent(const Event*);
	virtual glm::mat4 getCameraMatrix();
	void onEveryFrame();

	GLfloat cameraSpeed = 0.05f;
	GLfloat rotationSpeed = 0.3f;

	GLfloat yaw = -90.0f;
	GLfloat pitch = 0.0f;
};

class RotatingCamera : public Camera
{
public:
	RotatingCamera();
	~RotatingCamera();
	virtual glm::mat4 getCameraMatrix();
	GLfloat radius = 10.0f;
	GLfloat speed = 1.0f;
};