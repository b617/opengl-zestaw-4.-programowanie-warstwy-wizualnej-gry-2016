#pragma once
#include "Event.h"
#include "Light.h"
#include "..\glm\gtc\matrix_transform.hpp"

class Application{
private:
	Application(){
		light.lightColor = glm::vec3(1.0f, 0.7f, 0.7f);
		light.lightPosition = glm::vec3(0.0f, 25.0f, 0.0f);
	}
	~Application(){
	}
public:
	static Application & GetInstance(){
		static Application instance;
		return instance;
	}
	Application(Application const&) = delete;
	void operator=(Application const&) = delete;

	GLFWwindow * window;
	int windowSizeX = 800, windowSizeY = 800;
	float windowRatio = 1.0f * windowSizeX / windowSizeY;
	std::string windowTitle = "OpenGL window with GLFW, gl3w and glm";
	Light light;
	bool CreateNewWindow(){

		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

		glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		window = glfwCreateWindow(windowSizeX, windowSizeY, windowTitle.c_str(), NULL, NULL);
		return (window != NULL);
	}

	glm::mat4 getPerspectiveMatrix()
	{
		return glm::perspective(45.0f, windowRatio, 0.1f, 100.0f);
	}

};

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	KeyboardEvent event(window, key, scancode, action, mods);
}
static void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	MouseEvent event(window, xpos, ypos);
}
static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	MouseEvent event(window, button, action, mods);
}
