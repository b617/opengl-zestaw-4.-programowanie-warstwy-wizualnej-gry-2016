#pragma once
#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"
#include "gl3w.h"
#include <vector>
#include <string>
#include "system\Shader.h"
#include "system\Light.h"

#define M_PI 3.14159265358979323846
class Vertex;

class Mesh
{
public:
	Mesh(Vertex* vertices, unsigned int numVertices, GLuint drawingMode = GL_TRIANGLES);
	Mesh(std::vector<Vertex> & vertices, GLuint drawingMode = GL_TRIANGLES);

	void Draw();

	Mesh & setDrawingMode(GLuint value);

	virtual ~Mesh();
protected:
	enum
	{
		POSITION_VB,
		NORMALS,

		NUM_BUFFERS
	};
	GLuint mVertexArrayObject;
	GLuint mVertexArrayBuffers[NUM_BUFFERS];
	unsigned int mDrawCount;
	GLuint mDrawingMode = GL_TRIANGLES;
};

class HeightMap
{
public:
	HeightMap(const char* filepath);
	void Draw();
	void SendDataToShaderAndBind(Light light, glm::mat4 camera);
	~HeightMap();

	glm::mat4 modelMatrix = glm::mat4(1.0f);
protected:
	enum 
	{
		POSITION,
		NORMALS,
		FACES,
		NUM_BUFFERS
	};
	GLuint VAO;
	GLuint VBO[NUM_BUFFERS];
	GLuint texture;
	Shader * shader;
	unsigned int numVertices;
	unsigned int numIndices;
};

class Vertex{
public:
	Vertex(const glm::vec3& position) : pos(position){ }
	std::string toString(){
		return std::to_string(pos.x) + ", " + std::to_string(pos.y) + ", " + std::to_string(pos.z);
	}
	glm::vec3 pos;
protected:
};

struct MeshWithShader{
	Mesh * mesh;
	Shader * shader;

	void ApplyTransformations(float translateX, float translateY, float rotationAngle, float scale){
		glm::mat4 transformation = glm::translate(glm::vec3(translateX, translateY, 0.0f))
			* glm::rotate(rotationAngle, glm::vec3(0.0f, 0.0f, 1.0f))
			* glm::scale(glm::vec3(scale, scale, 1.0f));
		glUniformMatrix4fv(glGetUniformLocation(shader->GetProgramID(), "transform"), 1, GL_FALSE, &transformation[0][0]);
		shader->Bind();
	}

	MeshWithShader(Mesh* mesh, Shader* shader){
		this->mesh = mesh;
		this->shader = shader;
	}
};

class MeshStructure
{
public:
	MeshStructure(){}
	std::vector<MeshWithShader> objects;
	void Draw(){
		for each (MeshWithShader var in objects) {
			if (var.shader != NULL) var.shader->Bind();
			var.mesh->Draw();
		}
	}

	virtual ~MeshStructure(){}	
};

namespace BASIC_SHAPES {
	namespace shapes2D {

		Mesh * RegularPolygon(float radius, unsigned int verticesCount);
		Vertex * RegularPolygonVertices(float radius, unsigned int verticesCount);
		Mesh * Square(float radius);
		Mesh * Triangle(float radius);

	}
	namespace shapes3D{
		MeshStructure * Cone(float radius, float height, unsigned int numVertices);
		MeshStructure * Cylinder(float radius, float height, unsigned int numVertices);
		MeshStructure * Sphere(float radius, unsigned int numVertices);
	}
}