#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>
#include <SOIL\SOIL.h>
#define LoadTextureFromFile(X) SOIL_load_OGL_texture(X, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);


/*Classes*/
#include "Mesh.h"
#include "system\Application.h"
#include "system\Camera.h"
#include "AssimpModel.h"

//---------------------------------------------------------
void StartGL();
void QuitGL();

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	Shader shader("shaders/testShader");
	GLuint shader_camera = glGetUniformLocation(shader.GetProgramID(), "camera");
	GLuint shader_modelMatrix = glGetUniformLocation(shader.GetProgramID(), "transform");
	GLuint shader_texture = glGetUniformLocation(shader.GetProgramID(), "tex");
	GLuint shader_lightPosition = glGetUniformLocation(shader.GetProgramID(), "light.lightPosition");
	GLuint shader_lightColor = glGetUniformLocation(shader.GetProgramID(), "light.lightColor");

	//MeshStructure* mesh2 = //BASIC_SHAPES::shapes3D::Sphere(1.0, 32);
	//						 BASIC_SHAPES::shapes3D::Cone(1.0, 2.0, 32);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	AssimpModel model("files/box.obj", "files/tiger2.jpg");
	FPSCamera * camera = new FPSCamera();


	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);
		camera->onEveryFrame();

		////////////////////////////////
		glm::mat4 cameraMatrix = application.getPerspectiveMatrix()*camera->getCameraMatrix();
		glUniformMatrix4fv(shader_camera, 1, GL_FALSE, &cameraMatrix[0][0]);
		glUniform3fv(shader_lightPosition, 1, &application.light.lightPosition[0]);
		glUniform3fv(shader_lightColor, 1, &application.light.lightColor[0]);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, model.texture);
		glUniform1i(shader_texture, 0);
		model.modelMatrix = glm::translate(glm::vec3(0,0,-10));
		glUniformMatrix4fv(shader_modelMatrix, 1, GL_FALSE, &model.modelMatrix[0][0]);
		shader.Bind();
		model.Render();

		

		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSetCursorPosCallback(application.window, mouse_callback);
	glfwSetMouseButtonCallback(application.window, mouse_button_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0.1, 0.1, 0.1, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}