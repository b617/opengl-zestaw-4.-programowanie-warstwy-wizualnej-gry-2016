#include "stdafx.h"
#include "AssimpModel.h"
#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>

#include <SOIL\SOIL.h>



AssimpModel::MeshData::MeshData(aiMesh* mesh)
{
	for (int i = 0; i < BUFFERS::NUM_BUFFERS;) VBO[i++] = NULL;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	elementCount = mesh->mNumFaces * 3;

	if (mesh->HasPositions())
	{
		float * vertices = new float[mesh->mNumVertices * 3];
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			vertices[3 * i] = mesh->mVertices[i].x;
			vertices[3 * i +1] = mesh->mVertices[i].y;
			vertices[3 * i +2] = mesh->mVertices[i].z;
		}
		glGenBuffers(1, &VBO[BUFFER_VERTEX]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[BUFFER_VERTEX]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices*sizeof(GLfloat), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		delete[] vertices;
	}

	if (mesh->HasNormals())
	{
		float* normals = new float[mesh->mNumVertices * 3];
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			normals[3 * i] = mesh->mVertices[i].x;
			normals[3 * i + 1] = mesh->mVertices[i].y;
			normals[3 * i + 2] = mesh->mVertices[i].z;
		}
		glGenBuffers(1, &VBO[BUFFER_NORMALS]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[BUFFER_NORMALS]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), normals, GL_STATIC_DRAW);

		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(2);

		delete[] normals;
	}

	if (mesh->HasFaces())
	{
		unsigned int * indices = new unsigned int[mesh->mNumFaces * 3];
		for (unsigned int i = 0; i < mesh->mNumFaces; ++i) {
			indices[i * 3] = mesh->mFaces[i].mIndices[0];
			indices[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
			indices[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
		}

		glGenBuffers(1, &VBO[BUFFER_INDEX]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[BUFFER_INDEX]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * mesh->mNumFaces * sizeof(GLuint), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(3);

		delete[] indices;
	}
	if (mesh->HasTextureCoords(0))
	{
		float *texCoords = new float[mesh->mNumVertices * 2];
		for (int i = 0; i < mesh->mNumVertices; ++i)
		{
			texCoords[i * 2] = mesh->mTextureCoords[0][i].x;
			texCoords[i * 2 + 1] = mesh->mTextureCoords[0][i].y;
		}

		glGenBuffers(1, &VBO[BUFFER_TEXCOORD]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[BUFFER_TEXCOORD]);
		glBufferData(GL_ARRAY_BUFFER, 2 * mesh->mNumVertices * sizeof(GLfloat), texCoords, GL_STATIC_DRAW);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);

		delete[] texCoords;
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
AssimpModel::MeshData::~MeshData()
{
	if (VBO[BUFFER_VERTEX]) glDeleteBuffers(1, &VBO[BUFFER_VERTEX]);
	if (VBO[BUFFER_NORMALS]) glDeleteBuffers(1, &VBO[BUFFER_NORMALS]);
	if (VBO[BUFFER_TEXCOORD]) glDeleteBuffers(1, &VBO[BUFFER_TEXCOORD]);
	if (VBO[BUFFER_INDEX]) glDeleteBuffers(1, &VBO[BUFFER_INDEX]);

	glDeleteVertexArrays(1, &VAO);
}
void AssimpModel::MeshData::Render()
{
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, elementCount, GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);
}

AssimpModel::AssimpModel(const char* filename, const char* textureFilename)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(filename,aiProcess_GenNormals | aiProcess_GenUVCoords/*, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices*/);

	if (!scene){
		printf("Cannot load mesh: %s\nError info: %s\n", filename, importer.GetErrorString());
	}

	for (unsigned int i = 0; i < scene->mNumMeshes; i++) meshData.push_back(new AssimpModel::MeshData(scene->mMeshes[i]));

	texture = SOIL_load_OGL_texture(textureFilename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 
		SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_TEXTURE_REPEATS);
	/*int width, height, channels;
	unsigned char* pixelPtr = SOIL_load_image(textureFilename, &width, &height, &channels, SOIL_LOAD_RGBA);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelPtr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	delete[] pixelPtr;*/

	modelMatrix = glm::mat4(1.0f);

}
AssimpModel::~AssimpModel()
{
	for each (MeshData* var in meshData) delete var;
	meshData.clear();
}
void AssimpModel::Render()
{
	for each (MeshData* var in meshData) var->Render();
}