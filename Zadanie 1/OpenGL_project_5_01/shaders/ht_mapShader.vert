#version 140
 
uniform mat4 camera;
uniform mat4 transform;
uniform float maxHeight;
uniform float minHeight;

in vec3 position;
in vec3 normal;

out vec3 fragPosition;
out vec3 fragNormal;
out float coord;

void main()
{
	coord=(position.z-minHeight)/(maxHeight-minHeight);
	
	fragPosition=position;
	fragNormal=normal;
	//fragColor=texture(tex,coord);
	//fragColor=vec4(1,0,1,1);

    gl_Position = camera * transform * vec4(position,1.0);
}