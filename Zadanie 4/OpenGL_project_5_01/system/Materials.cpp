#pragma once
#include "gl3w.h"
struct Material{
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float shine;
	Material(float a1, float a2, float a3, float a4,
		float d1, float d2, float d3, float d4,
		float s1, float s2, float s3, float s4, float shine)
	{
		ambient[0] = a1;
		ambient[1] = a2;
		ambient[2] = a3;
		ambient[4] = a4;

		diffuse[0] = d1;
		diffuse[1] = d2;
		diffuse[2] = d3;
		diffuse[3] = d4;

		specular[0] = s1;
		specular[1] = s2;
		specular[2] = s3;
		specular[3] = s4;

		this->shine = shine;
	}

	void Bind()
	{
		/*
		glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, specular);		
		//shine???
		*/
	}
};

namespace Materials{
	static Material Brass(0.329412f, 0.223529f, 0.027451f, 1.0f,
		0.780392f, 0.568627f, 0.113725f, 1.0f,
		0.992157f, 0.941176f, 0.807843f, 1.0f,
		27.8974f);
	static Material Bronze(0.2125f, 0.1275f, 0.054f, 1.0f,
		0.714f, 0.4284f, 0.18144f, 1.0f,
		0.393548f, 0.271906f, 0.166721f, 1.0f,
		25.6f);
	static Material Chrome(0.25f, 0.25f, 0.25f, 1.0f,
		0.4f, 0.4f, 0.4f, 1.0f,
		0.774597f, 0.774597f, 0.774597f, 1.0f,
		76.8f);
	static Material Gold(0.24725f, 0.1995f, 0.0745f, 1.0f,
		0.75164f, 0.60648f, 0.22648f, 1.0f,
		0.628281f, 0.555802f, 0.366065f, 1.0f,
		51.2f);
	static Material Silver(0.19225f, 0.19225f, 0.19225f, 1.0f,
		0.50754f, 0.50754f, 0.50754f, 1.0f,
		0.508273f, 0.508273f, 0.508273f, 1.0f,
		51.2f);
	static Material Obsidian(0.05375f, 0.05f, 0.06625f, 0.82f,
		0.18275f, 0.17f, 0.22525f, 0.82f,
		0.332741f, 0.328634f, 0.346435f, 0.82f,
		38.4f);
}