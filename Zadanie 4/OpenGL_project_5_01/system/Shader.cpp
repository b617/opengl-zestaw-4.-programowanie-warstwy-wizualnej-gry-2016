#include "stdafx.h"
#include "Shader.h"
#include <fstream>


Shader::Shader(const std::string& fileName) {
	mProgram = glCreateProgram();
	mShaders[0] = CreateShader(FileToString(fileName + ".vert"), GL_VERTEX_SHADER);
	mShaders[1] = CreateShader(FileToString(fileName + ".frag"), GL_FRAGMENT_SHADER);

	for (unsigned int i = 0; i < NUM_SHADERS; i++) glAttachShader(mProgram, mShaders[i]);

	glBindAttribLocation(mProgram, 0, "position");


	glLinkProgram(mProgram);
	CheckShaderError(mProgram, GL_LINK_STATUS, true, "Error: shader program \"" + fileName + "\" failed to link!");

	glValidateProgram(mProgram);
	CheckShaderError(mProgram, GL_VALIDATE_STATUS, true, "Error: shader program \"" + fileName + "\" is invalid!");
}


Shader::~Shader() {

	for (unsigned int i = 0; i < NUM_SHADERS; i++) {
		glDetachShader(mProgram, mShaders[i]);
		glDeleteShader(mShaders[i]);
	}

	glDeleteProgram(mProgram);
}

std::string Shader::FileToString(const std::string & fileName){
	std::ifstream file;
	file.open((fileName).c_str());
	std::string output;
	std::string line;
	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		fputs(("Unable to load shader: " + fileName).c_str(), stderr);
		system("pause");
	}
	return output;
}

void Shader::CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };
	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);
	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		fputs((errorMessage + ": " + error + "'" + '\n').c_str(), stderr);
	}
}

void Shader::Bind() {
	glUseProgram(mProgram);
}

GLuint Shader::CreateShader(std::string text, GLenum shaderType) {
	GLuint shader = glCreateShader(shaderType);

	if (shader == 0) fputs("Error: Shader creation failed", stderr);

	const GLchar* shaderSource[1];
	shaderSource[0] = text.c_str();
	GLint shaderSourceLenghts[1];
	shaderSourceLenghts[0] = text.length();

	glShaderSource(shader, 1, shaderSource, shaderSourceLenghts);
	glCompileShader(shader);

	CheckShaderError(shader, GL_COMPILE_STATUS, false, "Shader compilation falied"/* +":\n"+text+'\n'*/);

	return shader;
}

GLuint Shader::GetProgramID(){
	return mProgram;
}