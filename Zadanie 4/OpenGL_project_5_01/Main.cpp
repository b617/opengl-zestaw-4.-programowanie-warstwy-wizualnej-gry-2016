#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>
#include <SOIL\SOIL.h>
#define LoadTextureFromFile(X) SOIL_load_OGL_texture(X, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);


/*Classes*/
#include "Mesh.h"
#include "system\Application.h"
#include "system\Camera.h"
#include "AssimpModel.h"

//---------------------------------------------------------
void StartGL();
void QuitGL();

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	FPSCamera * camera = new FPSCamera();
	HeightMap ht_map("files/terrain/terrain.png");
	ht_map.modelMatrix = glm::scale(glm::vec3(0.01))*ht_map.modelMatrix;


	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);
		camera->onEveryFrame();

		////////////////////////////////
		glm::mat4 cameraMatrix = application.getPerspectiveMatrix()*camera->getCameraMatrix();
		
		ht_map.SendDataToShaderAndBind(application.light, cameraMatrix);
		ht_map.Draw();
		
		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSetCursorPosCallback(application.window, mouse_callback);
	glfwSetMouseButtonCallback(application.window, mouse_button_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(1, 1, 1, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}