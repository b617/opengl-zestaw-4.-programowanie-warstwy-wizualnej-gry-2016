#version 140

uniform sampler2D tex;
uniform struct Light
{
	vec3 lightPosition;
	vec3 lightColor;
} light;

in vec3 fragNormal;
in vec3 fragPosition;
in float coord;

void main()
{
	vec3 ambientColor=0.3f*light.lightColor;
	
	vec3 normal=normalize(fragNormal);
	vec3 lightDirection=normalize(light.lightPosition-fragPosition);
	
	float brightness=max(dot(normal,lightDirection),0.0);
	vec3 diffuse = brightness*light.lightColor;
	
	vec3 outputColor=(ambientColor+diffuse)*vec3(1.0);
	
	vec4 texPixel=texture(tex,vec2(coord,0));
	//gl_FragColor=vec4(outputColor*fragColor.rgb,fragColor.a);
	//gl_FragColor=vec4(1,0,0,1);
	gl_FragColor=texPixel;
}